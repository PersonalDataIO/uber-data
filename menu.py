from IPython.core.display import display, HTML
from processing_widget import main_widget
from hours_analysis import weekly_hours


display(HTML(
"""
<h1> Calcul d'heures supplementaires (v 1.2)</h1>
<br>
Ceci est un service mis en place par l'ONG PersonalData.IO.
    <br>
Cet outil permet a un chauffeur Uber de calculer rapidement ses heures supplementaires, a partir d'un fichier obtenu de Uber.
<br>
<br>

Pour plus d'informations, vous pouvez aller voir ici:

<br>
<a href="https://forum.personaldata.io/t/demandez-vos-donnees-a-uber/309">https://forum.personaldata.io/t/demandez-vos-donnees-a-uber/309</a>
<br>
Ou rejoindre d'autres chauffeurs sur les groupes 
<ul>
    <li>WhatsApp: <a href="https://tinyurl.com/uber-whatsapp">https://tinyurl.com/uber-whatsapp</a></li>
    <li>Telegram: <a href="https://tinyurl.com/uber-telegram">https://tinyurl.com/uber-telegram</a></li>
    </ul>
<br>
Les calculs ci-dessous sont faits sans aucune garantie. 
<br>
<br>
Veuillez uploader le fichier .7z recu de Uber:
"""
    ))
display(main_widget(weekly_hours))