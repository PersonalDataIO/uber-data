import ipywidgets as widgets
import py7zr, os, zipfile

import time

import logging

timestamp = str(time.time())

upload_path = timestamp + "/upload/"
py7zr_path = timestamp + "/py7zr/"
uncompress_path = timestamp + "/uncompress/"        
import os
os.mkdir(timestamp) 
os.mkdir(upload_path)

logging.basicConfig(filename=timestamp+'/run.log',level=logging.DEBUG)

def batch(path, action, file_filter = lambda _: True):
    """
        Batch processes the files in a path, performing `action` on them, if `file_filter` returns True
    """
    logging.info(f"Batch run {path},{action}")
    for filename in sorted(os.listdir(path)):
        if file_filter(path + filename):
            action(path + filename)

def content_dir(path, verbose = True):
    """
        Prints names of files in path
    """
    logging.info(f"Content {path}")
    if verbose:
        print("Files and directories found inside {}: ".format(path))
    batch(path, lambda filename: print("    ", filename))

def unSevenZipPath(source_path, destination_path, password):
    """
        Uncompresses password protected files compressed according to 7z from source_path to destination_path
    """
    logging.info(f"unSevenZipPath {source_path},{destination_path}")
    import os
    os.mkdir(destination_path)
    batch(source_path, 
          lambda filename: unSevenZipFile(filename, destination_path, password), 
          file_filter = lambda filename: filename.endswith(".7z"))
    print("Un7z all files")
    content_dir(destination_path)
    
def unSevenZipFile(source_file, destination_path, password):
    print("Opening up the 7z file...", source_file)
    archive = py7zr.SevenZipFile(source_file, mode='r', password=password)
    archive.extractall(path=destination_path)
    archive.close()
    print("    ... Done")

def unZipPath(source_path, destination_path):
    import os
    os.mkdir(destination_path)
    batch(source_path, 
          lambda filename: unZipFile(filename, destination_path), 
          file_filter = lambda filename: filename.endswith(".zip"))
    print("Unzip all files")
    content_dir(destination_path)
        
def unZipFile(source_file, destination_path):
    print("Opening up the zip file...", source_file)
    archive = zipfile.ZipFile(source_file, mode='r')
    archive.extractall(path=destination_path)
    archive.close()
    print("    ... Done")

def move_folder(source_path, destination_path):
    logging.info(f"Move folder {source_path},{destination_path}")
    import shutil
    shutil.copy(source_path, destination_path)

def save_uploaded_file(data, upload_path, filename):
    with open(upload_path+filename, "wb") as f:
        f.write(data)

def upload_widget(callback):
    widget = widgets.FileUpload(
        description="Upload du fichier 7z",
        accept='.7z',
        multiple=False,
        layout={'width': 'max-content'}
    )
    def _handle_upload(change):
        print("Upload en cours...")
        w = change['owner']
        for i, d in enumerate(w.data):
            save_uploaded_file(d, upload_path, w.metadata[i]["name"])
        print("Upload accompli!")
        callback()

    widget.observe(_handle_upload, names='data')
    return widget    

def next_step_fn(fn):
    def next_step():
        print("Veuillez rentrer le mot de passe fourni par Uber via un deuxieme email")
        pw_widget = password_widget()
        display(pw_widget, go_widget(pw_widget, fn))
    return next_step  # A widget asking for password and a go button, executing "fn"

def main_widget(fn):
    return upload_widget(next_step_fn(fn))

def password_widget():
    widget = widgets.Text(
        value='',
        placeholder='password',
        description='',
        disabled=False
    )
    return widget

def uncompress(upload_path, uncompress_path, py7zr_path, password):
    unSevenZipPath(upload_path, py7zr_path, password)
    unZipPath(py7zr_path, uncompress_path)
    if len(os.listdir(uncompress_path)) == 1: # copy files one directory down
        subfolder = os.listdir(uncompress_path)[0]
        import shutil
        for filename in os.listdir(uncompress_path +"/" +subfolder):
            try:
                os.makedirs(uncompress_path +subfolder + "/")
            except:
                pass
            shutil.move(uncompress_path + subfolder + "/" + filename, uncompress_path)
            logging.info(f"Moved {filename} one directory down")
        shutil.rmtree(uncompress_path +subfolder)
    import shutil
    # Also take the csv files with you!
    batch(py7zr_path, lambda filename: shutil.copy(filename, uncompress_path), file_filter = lambda filename: filename.endswith(".csv"))
    print("*"*80)
    print("List of files found in the archive (copy/paste this if problems later):")
    for f in os.listdir(uncompress_path):
        print(f)
    print("*"*80)    
        

def go_widget(pw_widget, compute_fn):
    widget = widgets.Button(description="Calculez mes heures supplementaires!", layout={'width': 'max-content'})
    def _handle_go(change):
        uncompress(upload_path, uncompress_path, py7zr_path, pw_widget.value)
        print("Analyzing the data...")
        print(compute_fn(uncompress_path))

    widget.on_click(_handle_go)
    return widget
    
